{ buildGoModule }:

let
  pname = "create-project";
  version = "0.0.1";
in buildGoModule {
  inherit pname version;

  src = ./.;

  vendorHash = null;
}
