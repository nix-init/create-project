package main

import (
    "fmt"
    "os"
    "os/exec"
    "strings"
    "flag"
    "errors"
    "net/http"
    "io"
)

type Project struct {
    Name string
    Type string
}

func cloneRepo(s, g string) {
    cmd := exec.Command("git", "clone", g + "/" + s + "-init")

    out, err := cmd.Output()
    if err != nil {
        fmt.Println("cound not run command: ", err)
    }

    fmt.Println(string(out))
}

func writeLicense(n, l string) {
    dir := "./" + n

    license := strings.TrimSpace(l)
    license = strings.ToLower(license)
    license = strings.Replace(license, " ", "-", -1)
    license = strings.Replace(license, ".0", "", -1)

    url := ""

    switch license {
    // Get more licenses at: https://opensource.org/licenses
        case "none":
            // do nothing b/c all rights reserved
        case "agpl-3-only", "agplv3-only", "agpl3-only":
            // fix standard license header
            url = "https://www.gnu.org/licenses/agpl-3.0.txt"
        case "agpl-3-or-later", "agplv3-or-later", "agpl3-or-later", "agpl-3-later", "agplv3-later", "agpl3-later", "agpl-3+", "agplv3+", "agpl3+", "agpl+", "agpl-3", "agplv3", "agpl3", "agpl":
            // fix standard license header
            url = "https://www.gnu.org/licenses/agpl-3.0.txt"
        case "gpl-3-only", "gplv3-only", "gpl3-only":
            // fix standard license header
            url = "https://www.gnu.org/licenses/gpl-3.0.txt"
        case "gpl-3-or-later", "gplv3-or-later", "gpl3-or-later", "gpl-3-later", "gplv3-later", "gpl3-later", "gpl-3+", "gplv3+", "gpl3+", "gpl+", "gpl-3", "gplv3", "gpl3", "gpl":
            // fix standard license header
            url = "https://www.gnu.org/licenses/gpl-3.0.txt"
        case "gpl-2-only", "gplv2-only", "gpl2-only":
            // fix standard license header
            url = "https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt"
        case "gpl-2-or-later", "gplv2-or-later", "gpl2-or-later", "gpl-2-later", "gplv2-later", "gpl2-later", "gpl-2+", "gplv2+", "gpl2+", "gpl-2", "gplv2", "gpl2":
            // fix standard license header
            url = "https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt"
        case "lgpl-3-only", "lgplv3-only", "lgpl3-only":
            // fix standard license header
            url = "https://www.gnu.org/licenses/lgpl-3.0.txt"
        case "lgpl-3-or-later", "lgplv3-or-later", "lgpl3-or-later", "lgpl-3-later", "lgplv3-later", "lgpl3-later", "lgpl-3+", "lgplv3+", "lgpl3+", "lgpl+", "lgpl-3", "lgplv3", "lgpl3", "lgpl":
            // fix standard license header
            url = "https://www.gnu.org/licenses/lgpl-3.0.txt"
        case "lgpl-2-only", "lgplv2-only", "lgpl2-only":
            // fix standard license header
            url = "https://www.gnu.org/licenses/old-licenses/lgpl-2.0.txt"
        case "lgpl-2-or-later", "lgplv2-or-later", "lgpl2-or-later", "lgpl-2-later", "lgplv2-later", "lgpl2-later", "lgpl-2+", "lgplv2+", "lgpl2+", "lgpl-2", "lgplv2", "lgpl2":
            // fix standard license header
            url = "https://www.gnu.org/licenses/old-licenses/lgpl-2.0.txt"
        case "mpl2", "mpl", "mozilla2", "mozilla":
            url = "https://www.mozilla.org/en-US/MPL/2.0/"
        case "apache-2", "apache-1.1", "apache-1", "apache2", "apache1", "apache":
            url = "https://www.apache.org/licenses/LICENSE-2.0.txt"
        case "mit", "expat":
            url = "https://www.mit.edu/~amini/LICENSE.md"
        case "x11":
            url = "https://raw.githubusercontent.com/XQuartz/XQuartz/main/pkg/resources/LICENSE.txt"
        case "mit-no-attribution", "mit-no-attrib", "mit-no-attr", "mit-0", "mit0":
            url = "https://raw.githubusercontent.com/aws/mit-0/master/MIT-0"
        case "unlicense":
            url = "https://unlicense.org/UNLICENSE"
        case "prior-bsd":
            url = ""
        case "original-bsd", "4-clause-bsd", "4bsd", "bsd":
            url = ""
        case "revised-bsd", "new-bsd", "modified-bsd", "3-clause-bsd", "3bsd", "bsd2":
            url = ""
        case "simplified-bsd", "freebsd", "2-clause-bsd", "2bsd":
            url = ""
        case "3-clause-bsd-nuclear", "3bsd-nuclear", "3bsd-n", "3bsdn":
            url = ""
        case "2-clause-bsd-patent", "2bsd-patent", "2bsd-p", "2bsdp":
            url = ""
        case "0-clause-bsd", "0bsd", "bsd0":
            url = ""
        case "bsl", "boost", "1-clause-bsd", "1bsd":
            url = ""
        case "3-clause-bsd-clear", "3bsd-clear":
            url = ""
        case "academic-free", "afl-3", "afl3", "afl-2.1", "afl2.1", "afl-2", "afl2", "afl-1.2", "afl1.2", "afl-1.1", "afl1.1", "afl":
            url = ""
        case "artistic license", "artistic":
            url = ""
        case "blue-oak-model", "blueoak-1", "blueoak1", "blueoak":
            url = ""
        case "creative-commons", "cc-by", "cc":
            url = ""
        case "creative-commons-share-alike", "cc-by-sa":
            url = ""
        case "creative-commons-noncommercial", "cc-by-nc":
            url = ""
        case "creative-commons-share-alike-noncommercial", "cc-by-sa-nc":
            url = ""
        case "creative-commons-no-derivatives", "cc-by-nd":
            url = ""
        case "creative-commons-noncommercial-no-derivatives", "cc-by-nc-nd":
            url = ""
        case "creative-commons-no-rights-reserved", "creative-commons-no-rights", "cc0":
            url = ""
        case "cecill-1", "cecill1", "cecill-1.1", "cecill1.1", "cecill-2", "cecill2", "cecill-2.1", "cecill2.1", "cecill-b", "cecill-c", "cecill":
            url = ""
        case "cern-ohl-s", "cern-ohl", "cern":
            url = ""
        case "cern-ohl-w":
            url = ""
        case "cern-ohl-p":
            url = ""
        case "ecl-2", "ecl":
            url = ""
        case "epl-1", "epl1":
            url = ""
        case "epl-2", "epl2", "epl":
            url = ""
        case "eupl-1.1", "eupl1.1":
            url = ""
        case "eupl-1.2", "eupl1.2", "eupl":
            url = ""
        case "gfdl-1.3", "gfdlv1.3", "gfdl1.3", "gfdl":
            url = ""
        case "isc":
            url = ""
        case "lppl-1.3c", "lpplv1.3c", "lppl1.3c", "lppl":
            url = ""
        case "ms-pl":
            url = ""
        case "ms-rl":
            url = ""
        case "mulanpsl-2", "mulanpsl2", "mulanpsl":
            url = ""
        case "ncsa", "uiuc":
            url = ""
        case "odbl-1", "odbl1", "odbl":
            url = ""
        case "sil", "ofl-1.1", "ofl-1", "ofl1.1", "ofl1", "ofl":
            url = ""
        case "osl-3", "osl3", "osl":
            url = ""
        case "postgresql":
            url = ""
        case "upl-1", "upl1", "upl":
            url = ""
        case "vim":
            url = ""
        case "wtfpl":
            url = ""
        case "zlib":
            url = ""
        default:
            fmt.Println("Error: license does not exist")
    }

    response, httpErr := http.Get(url)
    if httpErr != nil {
        panic(httpErr)
    }

    defer response.Body.Close()

    out, fileErr := os.Create(dir + "/LICENSE")
    if fileErr != nil {
        panic(fileErr)
    }

    defer out.Close()

    io.Copy(out, response.Body)
}

func readDerivation(s string) string {
    dat, err := os.ReadFile("./" + s + "-init/derivation.nix")

    if err != nil {
        panic(err)
    }

    return string(dat)
}

func replaceProjectName(file, name string) string {
    return strings.Replace(
        file,
        "<project-name>",
        name,
        -1,
    )
}

func getPermissions(file string) os.FileMode {
    stat, err := os.Stat(file)

    if err != nil {
        panic(err)
    }

    return stat.Mode()
}

func applyTemplates(p Project) {
    repo := "./" + p.Type + "-init"
    derivation := repo + "/derivation.nix"

    os.WriteFile(
        derivation,
        []byte(replaceProjectName(
            readDerivation(p.Type),
            p.Name,
        )),
        getPermissions(derivation),
    )

    os.Rename(repo, "./" + p.Name)
}

func setup(n string) {
    script := "./" + n + "/setup.sh"
    
    _, err := os.Stat(script)
    if err == nil {
        cmd := exec.Command(script)

        out, err := cmd.Output()
        if err != nil {
            fmt.Println("Error: could not run setup script:", err)
        }

        fmt.Println(string(out))
    } else if errors.Is(err, os.ErrNotExist) {
        // do nothing b/c setup.sh is optional
    } else {
        fmt.Println("Error: Schrödinger's setup script (may or may not exist):", err)
    }
}

func runWriter(w string) {
    if w != "none" {
        commands := strings.Split(w, " ")
        cmd := exec.Command(
            commands[0],
            commands[1],
        )

        out, err := cmd.Output()
        if err != nil {
            fmt.Println("Error: could not run script:", err)
        }

        fmt.Println(string(out))
    }
}

func postRun(s string) {
    if s != "none" {
        cmd := exec.Command(s)

        out, err := cmd.Output()
        if err != nil {
            fmt.Println("Error: could not run post-install script:", err)
        }

        fmt.Println(string(out))
    }
}

func main() {
    var license string;
    flag.StringVar(&license, "l", "none", "License of the project")
    flag.StringVar(&license, "license", "none", "License of the project")

    var git string;
    flag.StringVar(&git, "g", "https://gitlab.com/nix-init", "Alternative git URL to find init repository")
    flag.StringVar(&git, "git", "https://gitlab.com/nix-init", "Alternative git URL to find init repository")

    var script string;
    flag.StringVar(&script, "s", "none", "Optional script to run")
    flag.StringVar(&script, "script", "none", "Optional script to run")

    var writer string;
    flag.StringVar(&writer, "w", "none", "Alternative writers for README and such")
    flag.StringVar(&writer, "writer", "none", "Alternative writers for README and such")

    flag.Parse()

    args := flag.Args()
    proj := Project{
        args[0],
        args[1],
    }

    cloneRepo(proj.Type, git)
    writeLicense(proj.Name, license)
    applyTemplates(proj)
    setup(proj.Name) // optional setup.sh script
    runWriter(writer)
    postRun(script)
}
